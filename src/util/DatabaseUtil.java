package util;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.*;

/**
 * class to manage the comunication with the database
 *
 */
public class DatabaseUtil implements DatabaseHandler {
	public EntityManagerFactory entityManagerFactory;
	public EntityManager entityManager;
	
	public boolean isSetUp = false;
	private static DatabaseUtil singleton = new DatabaseUtil();
	private DatabaseUtil() {
		
	}
	
	public static DatabaseUtil getInstance() {
		if(!singleton.isSetUp)
			singleton.setup();
		return singleton;
	}
	/**
	 * initialize the database handlers
	 */
	@Override
	public void setup() {
		entityManagerFactory = Persistence.createEntityManagerFactory("AutoDealer2018");
		entityManager = entityManagerFactory.createEntityManager();
		singleton.isSetUp = true;
	}
	@Override
	public <T>void save(T entry){
		startTransaction();
		entityManager.persist(entry);
		entityManager.flush();
		commitTransaction();
	}
	/**
	 * save a client to the database
	 * @param client object to be saved
	 */
	public void saveClient(Client client) {
		entityManager.persist(client);
	}
	/**
	 * print the clients that are in the database
	 */
	public void printClients() {
		List<Client> clients = entityManager.createNativeQuery("SELECT * FROM auto.client", Client.class).getResultList();
		System.out.println("Lista clienti : ");
		System.out.println();
		clients.forEach((client)->{
			printClient(client);
			System.out.println();
		});
	}
	public void printClient(Client client){
		System.out.println("Nume : " + client.getNume());
		System.out.println("Prenume : " + client.getPrenume());
		printAdresa(client.getAdresa());
		System.out.println("CNP : " + client.getCnp());
		System.out.println("Serie Buletin : " + client.getSerieBuletin());
		System.out.println("Numar Buletin : " + client.getNumarBuletin());
		System.out.println("Numar Telefon : " + client.getNumarTelefon());
	}
	/**
	 * refresh a Client entry based on Id
	 * @param clientId primary key
	 */
	//public void updateClient(int clientId) {
		//Client a = entityManager.find(Client.class, clientId);
		//entityManager.refresh(a);
	//}
	/**
	 * remove an Client entry from the database based on Id
	 * clientId primary key
	 */
	public void removeClient(int clientId) {
		Client a = entityManager.find(Client.class, clientId);
		entityManager.remove(a);
	}
	/**
	 * save a car to the database
	 * @param masina car object to be saved
	 */
	public void saveMasina(Masina masina) {
		entityManager.persist(masina);
		entityManager.flush();
	}
	/**
	 * print the masini that are in the database
	 */
	public void printMasini() {
		List<Masina> masini = entityManager.createNativeQuery("SELECT * FROM auto.masina", Masina.class).getResultList();
		System.out.println("Lista masini : ");
		System.out.println();
		masini.forEach((masina)->{
			printMasina(masina);
			System.out.println();
		});
	}
	
	public void printMasina(Masina masina) {
		System.out.println("Marca : " + masina.getMarca());
		System.out.println("Model : " + masina.getModel());
		System.out.println("Culoare : " + masina.getCuloare());
		System.out.println("Data fabricatie : " + masina.getDataFabricatie());
		System.out.println("Capacitate cilindrica : " + masina.getCapacitateCilindrica());
		System.out.println("Cai putere : " + masina.getCaiPutere());
		System.out.println("Factor poluare : " + masina.getFactorDePoluare());
		System.out.println("Numar usi : " + masina.getNumarUsi());
		System.out.println("Numar locuri : " + masina.getNumarLocuri());
		System.out.println("Pret : " + masina.getPret());
		System.out.println("TVA : " + masina.getTva());
	}
	/**
	 * refresh a car entry based on Id
	 * @param masinaId primary key
	 */
	//public void updateMasina(int masinaId) {
		//Masina v = entityManager.find(Masina.class, masinaId);
//		entityManager.refresh(v);
//	}
	/**
	 * remove a car entry based on Id
	 * @param masinaId primary key
	 */

	public void removeMasina(int masinaId) {
		Masina v = entityManager.find(Masina.class, masinaId);
		entityManager.remove(v);
	}
	/**
	 * save an adress to the database
	 * @param adresa adress to be saved
	 */
	public void saveAdresa(Adresa adresa) {
		entityManager.persist(adresa);
	}
	
	/**
	 * print the adress list
	 */
	public void printAdrese() {
		List<Adresa> adrese = entityManager.createNativeQuery("SELECT * FROM auto.adresa", Adresa.class).getResultList();
		System.out.println("Lista adrese : ");
		System.out.println();
		adrese.forEach((adresa)-> {
			printAdresa(adresa);
			System.out.println();
	});
	}
	
	public void printAdresa(Adresa adresa) {
		System.out.println("Judet : " + adresa.getJudet());
		System.out.println("Localitate : " + adresa.getLocalitate());
		System.out.println("Strada : " + adresa.getStrada());
		System.out.println("Numar : " + adresa.getNumar());
		System.out.println("Cod postal : " + adresa.getCodPostal());
		System.out.println("Scara : " + adresa.getScara());
		System.out.println("Apartament : " + adresa.getNrApartament());
	}
	/**
	 * remove an adress entry from the database based on id
	 * @param adresaId primary key
	 */
	public void removeAdresa(int adresaId) {
		Adresa a = entityManager.find(Adresa.class, adresaId);
		entityManager.remove(a);
	}
	/**
	 * Selectively refresh an adress entry based on Id
	 * @param adresaId primary key
	 */
//	public void updateAdresa(int adresaId) {
//		Appointment a = entityManager.find(Adresa.class, adresaId);
//		entityManager.refresh(a);
//	}
	public void saveFabrica(Fabrica fabrica) {
		entityManager.persist(fabrica);
		entityManager.flush();
	}
	
	public void removeFabrica(int fabricaId) {
		Fabrica a = entityManager.find(Fabrica.class, fabricaId);
		entityManager.remove(a);
	}
	
	public void printFabrici() {
		List<Fabrica> fabrici = entityManager.createNativeQuery("SELECT * FROM auto.fabrica", Fabrica.class).getResultList();
		System.out.println("Lista fabrici : ");
		System.out.println();
		fabrici.forEach((fabrica)-> {
			printFabrica(fabrica);
			System.out.println();
	});
	}
	
	public void printFabrica(Fabrica fabrica) {
		System.out.println("Id : " + fabrica.getId());
		printAdresa(fabrica.getAdresa());
		printProducator(fabrica.getProducator());
	}
	
	public void saveProducator(Producator producator) {
		entityManager.persist(producator);
		entityManager.flush();
	}
	
	public void removeProducator(int producatorId) {
		Producator a = entityManager.find(Producator.class, producatorId);
		entityManager.remove(a);
	}
	
	public void printProducatori() {
		List<Producator> producatori = entityManager.createNativeQuery("SELECT * FROM auto.producator", Producator.class).getResultList();
		System.out.println("Lista producatori : ");
		System.out.println();
		producatori.forEach((producator)-> {
			printProducator(producator);
			System.out.println();
	});
	}
	
	public void printProducator(Producator producator) {
		System.out.println("Id : " + producator.getId());
		System.out.println("Nume : " + producator.getNume());
		printAdresa(producator.getAdresa());
		System.out.println("An infiintare : " + producator.getAnInfiintare());
		System.out.println("Numar telefon : " + producator.getNrTelefon());
	}
	
	public void saveStocMasina(StocMasini stocMasina) {
		entityManager.persist(stocMasina);
		entityManager.flush();
	}
	
	public void removeStocMasina(int stocMasinaId) {
		StocMasini a = entityManager.find(StocMasini.class, stocMasinaId);
		entityManager.remove(a);
	}
	
	public void printStocMasini() {
		List<StocMasini> stocMasini = entityManager.createNativeQuery("SELECT * FROM auto.stoc_masini", StocMasini.class).getResultList();
		System.out.println("Stoc masini : ");
		System.out.println();
		stocMasini.forEach((stocMasina)-> {
			printStocMasina(stocMasina);
			System.out.println();
	});
	}
	
	public void printStocMasina(StocMasini stocMasina) {
		System.out.println("Id : " + stocMasina.getId());
		printMasina(stocMasina.getMasina());
		printFabrica(stocMasina.getFabrica());
		System.out.println("Cantitate : " + stocMasina.getCantitate());
		System.out.println("Data Receptionare : " + stocMasina.getDataReceptionarii());
	}

	public void saveTranzactie(Tranzactie tranzactie) {
		entityManager.persist(tranzactie);
		entityManager.flush();
	}
	
	public void removeTranzactie(int tranzactieId) {
		Tranzactie a = entityManager.find(Tranzactie.class, tranzactieId);
		entityManager.remove(a);
	}
	
	public void printTranzactii() {
		List<Tranzactie> tranzactii = entityManager.createNativeQuery("SELECT * FROM auto.tranzactie", Tranzactie.class).getResultList();
		System.out.println("Lista Tranzactii : ");
		System.out.println();
		tranzactii.forEach((tranzactie)-> {
			printTranzactie(tranzactie);
			System.out.println();
	});
	}
	
	public void printTranzactie(Tranzactie tranzactie) {
		System.out.println("Id : " + tranzactie.getId());
		printMasina(tranzactie.getMasina());
		printClient(tranzactie.getClient());
		System.out.println("Data : " + tranzactie.getData());
		System.out.println("Suma : " + tranzactie.getSuma());
		System.out.println("Perioada garantie : " + tranzactie.getPerioadaGarantie());
	}
	/**
	 * print the transactions in chronological order
	 */

	
	/**
	 * start a transaction with the database
	 */
	public void startTransaction() {
		entityManager.getTransaction().begin();
	}
	/**
	 * commit the transaction
	 */
	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}
	/**
	 * close the connection with the database
	 */
	@Override
	public void stopEntityManager() {
		singleton.isSetUp = false;
		entityManager.close();
	}
}
