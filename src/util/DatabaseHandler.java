package util;

public interface DatabaseHandler {
	public abstract void setup();
	public abstract <T>void save(T entry);
	public abstract void stopEntityManager();
}
