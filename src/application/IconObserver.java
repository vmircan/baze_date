package application;

import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.ImageView;

public class IconObserver {
	private ImageView icon;
	private IconSubject subject;
	public IconObserver(ImageView icon, IconSubject subject) {
		this.icon=icon;
		this.subject=subject;
	}
	void update() {
		ColorAdjust colorAdjust = new ColorAdjust();
		
		colorAdjust.setBrightness(0.3);
		icon.setEffect(colorAdjust);
		
	}
}
