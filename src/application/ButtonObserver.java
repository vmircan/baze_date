package application;

import javafx.scene.control.Button;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class ButtonObserver {
	private Button button;
	private ButtonSubject subject;
	public ButtonObserver(Button button, ButtonSubject subject) {
		this.button=button;
		this.subject=subject;
	}
	void update() {
		button.setFont(Font.font("Helvetica", FontWeight.THIN, subject.getFontSize()));
		button.setStyle("-fx-text-fill: " + subject.getHexColorString() + ";");
	}
}
