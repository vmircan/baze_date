package application;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class ButtonSubject {
	private List<ButtonObserver> buttons = new ArrayList<ButtonObserver>();
	
	static private int defaultFontSize=22;
	
	private int fontSize = defaultFontSize;
	
	private static Color defaultColor = Color.WHITE;
	
	private Color color = defaultColor;
	
	public void attach(ButtonObserver buttonObserver) {
		buttons.add(buttonObserver);
	}
	public int getFontSize() {
		return fontSize;
	}
	
	public void setFontSize(int fontSize) {
		this.fontSize=fontSize;
	}
	public void setColor(Color color) {
		this.color=color;
	}
	public Color getColor() {
		return color;
	}
	public void notifyAllObservers() {
		for(ButtonObserver buttonObserver :buttons) {
			buttonObserver.update();
		}
	}
	public void resetToDefaults() {
		fontSize=defaultFontSize;
		color=defaultColor;
	}
	public String getHexColorString() {
		return String.format("#%02X%02X%02X", (int)(color.getRed()*255),
				(int)(color.getGreen()*255),
				(int)(color.getBlue()*255));
	}
}
