package application;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.paint.Color;

public class IconSubject {
	private List<IconObserver> icons = new ArrayList<IconObserver>();
	
	private static Color defaultColor = Color.WHITE;
	
	private Color color = defaultColor;
	
	public void attach(IconObserver iconObserver) {
		icons.add(iconObserver);
	}
	public void setColor(Color color) {
		this.color=color;
	}
	public Color getColor() {
		return color;
	}
	public void notifyAllObservers() {
		for(IconObserver iconObserver :icons) {
			iconObserver.update();
		}
	}
	public void resetToDefaults() {
		color=defaultColor;
	}
	public String getHexColorString() {
		return String.format("#%02X%02X%02X", (int)(color.getRed()*255),
				(int)(color.getGreen()*255),
				(int)(color.getBlue()*255));
	}
}
