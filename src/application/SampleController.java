package application;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXColorPicker;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXScrollPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.util.Pair;
import javafx.util.StringConverter;
import util.DatabaseUtil;
import model.*;

public class SampleController {
	@FXML
    private AnchorPane main;

    @FXML
    private AnchorPane createTransaction;

    @FXML
    private JFXTextField JTNumeClient;

    @FXML
    private JFXTextField JTSerieBuletinClient;

    @FXML
    private JFXTextField JTCNPClient;

    @FXML
    private JFXTextField JTStradaClient;

    @FXML
    private JFXTextField JTJudetClient;

    @FXML
    private JFXTextField JTPrenumeClient;

    @FXML
    private JFXTextField JTNumarBuletinClient;

    @FXML
    private JFXTextField JTTelefonClient;

    @FXML
    private JFXTextField JTNumarStradaClient;

    @FXML
    private JFXTextField JTCodPostalClient;

    @FXML
    private JFXTextField JTPrice;

    @FXML
    private JFXComboBox<Masina> JCBComboBox;

    @FXML
    private JFXTextField JTLocalitateClient;

    @FXML
    private AnchorPane showTransaction;

    @FXML
    private JFXTextField JTNumeClient2;

    @FXML
    private JFXTextField JTCNPClient2;

    @FXML
    private JFXTextField JTStradaClient2;

    @FXML
    private JFXTextField JTJudetClient2;

    @FXML
    private JFXTextField JTLocalitateClient2;

    @FXML
    private JFXTextField JTCuloare;

    @FXML
    private JFXTextField JTCapacitateCilindrica;

    @FXML
    private JFXTextField JTCaiPutere;

    @FXML
    private JFXTextField JTFactorPoluare;

    @FXML
    private JFXTextField JTCodPostalClient2;

    @FXML
    private JFXTextField JTPrice2;

    @FXML
    private JFXTextField JTNumarUsi;

    @FXML
    private JFXTextField JTNumarBuletinClient2;

    @FXML
    private JFXTextField JTMasina;

    @FXML
    private JFXTextField JTNumarLocuri;

    @FXML
    private JFXTextField JTDataFabricatie;

    @FXML
    private JFXTextField JTSerieBuletinClient2;

    @FXML
    private JFXTextField JTNumarTelefonClient2;
    
    @FXML
    private AnchorPane JAPSettings;
    
    @FXML
    private JFXComboBox<Integer> JCBFontSize;
    
    @FXML 
    private JFXColorPicker JCPColorPicker;
   
    @FXML 
    private JFXColorPicker JCPSidebarColor;
    
    @FXML
    private JFXColorPicker JCPIconColor;
    
    @FXML
    private ImageView JIHome;
    
    @FXML
    private ImageView JISettingsIcon;

    @FXML
    private AnchorPane JAPSidebar;

    @FXML
    private Button toCreateTransactoin;

    @FXML
    private VBox sidebarVBox;
    
    @FXML
    private Button JBSettingsButton;

    
    private int counter;

    private List<Tranzactie> transactions=new ArrayList<Tranzactie>();
    private ArrayList<Pair<Integer, JFXButton>> buttons=new ArrayList<Pair<Integer, JFXButton>>();
    
    private ButtonSubject buttonSubject = new ButtonSubject();
    
    private Pane activePane = createTransaction;
    
    private static Color defaultSidebarColor = Color.color(0.0, 0.0, 0.26);
    
    private Color sidebarColor = defaultSidebarColor;
    
    
    
    @FXML
    public void returnToCreateTransaction() {
    	if(activePane!=createTransaction) {
    		createTransaction.toFront();
    		createTransaction.setVisible(true);
    		createTransaction.setDisable(false);
    		activePane.setDisable(true);
    		activePane.setVisible(false);
    		activePane = createTransaction;
    	}
    }
	@FXML
	public void printAppointments() {
		DatabaseUtil  db = DatabaseUtil.getInstance();
		db.printTranzactii();
		db.stopEntityManager();
	}
	
	@FXML 
	public void toSettings(){
		if(activePane!=JAPSettings) {
			JAPSettings.toFront();
			JAPSettings.setVisible(true);
			JAPSettings.setDisable(false);
			activePane.setDisable(true);
			activePane.setVisible(false);
			activePane = JAPSettings;
			JCBFontSize.setValue(buttonSubject.getFontSize());
			JCPColorPicker.setValue(buttonSubject.getColor());
			JCPSidebarColor.setValue(sidebarColor);
		}
	}
	
	@FXML
	public void createTransaction() {
		Tranzactie tranzactie = new Tranzactie();
		Adresa adresa = new Adresa();
		Masina masina = (Masina)JCBComboBox.getSelectionModel().getSelectedItem();
		adresa.setStrada(JTStradaClient.getText());
		adresa.setNumar(Integer.parseInt(JTNumarStradaClient.getText()));
		adresa.setJudet(JTJudetClient.getText());
		adresa.setLocalitate(JTLocalitateClient.getText());
		Client client = new Client(JTNumeClient.getText(), JTPrenumeClient.getText(), adresa, JTCNPClient.getText(),
				JTSerieBuletinClient.getText(), JTNumarBuletinClient.getText(), JTTelefonClient.getText());
		tranzactie.setClient(client);
		tranzactie.setMasina(masina);
		tranzactie.setData("2019-01-17");
		tranzactie.setPerioadaGarantie(2);
		tranzactie.setSuma(Double.toString((masina.getPret()*1000 + masina.getPret()*200)/1000));
		DatabaseUtil  db = DatabaseUtil.getInstance();
		db.save(adresa);
		db.save(client);
		db.save(tranzactie);
		transactions.add(tranzactie);
		JFXButton newButton= new JFXButton("Transaction "+counter);
		//buttons.add(new Pair<Integer, Button>(counter, newButton));
		newButton.setTextAlignment(TextAlignment.CENTER);
		//newButton.set
		newButton.setStyle(
				"-fx-text-fill:#fff;");
		newButton.setFont(Font.font("Helvetica", FontWeight.THIN, 22));
		newButton.setOnAction(new EventHandler<ActionEvent>() {
			final Tranzactie t=tranzactie;
			Client c = client;
			Masina m = masina;
			Adresa a = adresa;
			@Override public void handle(ActionEvent e) {
				JTNumeClient2.setText(c.getNume() + " " +c.getPrenume());
				JTSerieBuletinClient2.setText(c.getSerieBuletin());
				JTNumarBuletinClient2.setText(c.getNumarBuletin());
				JTCNPClient2.setText(c.getCnp());
				JTNumarTelefonClient2.setText(c.getNumarTelefon());
				JTStradaClient2.setText(a.getStrada() + " " + a.getNumar());
				JTJudetClient2.setText(a.getJudet());
				JTLocalitateClient2.setText(a.getLocalitate());
				JTCodPostalClient2.setText(Integer.toString(a.getCodPostal()));
				JTMasina.setText(m.getMarca() + " " +m.getModel());
				JTCuloare.setText(m.getCuloare());
				JTCapacitateCilindrica.setText(Integer.toString(m.getCapacitateCilindrica()));
				JTCaiPutere.setText(Integer.toString(m.getCaiPutere()));
				JTFactorPoluare.setText(m.getFactorDePoluare());
				JTNumarUsi.setText(Integer.toString(m.getNumarUsi()));
				JTNumarLocuri.setText(Integer.toString(m.getNumarLocuri()));
				JTDataFabricatie.setText(m.getDataFabricatie());
				JTPrice2.setText(t.getSuma() + " RON");
				if(activePane!=showTransaction) {
					showTransaction.toFront();
					showTransaction.setVisible(true);
					showTransaction.setDisable(false);
					activePane.setDisable(true);
					activePane.setVisible(false);
					activePane=showTransaction;
				}
			}
		});
		boolean notAdded=true;
		for(int i=0; i<buttons.size() && notAdded; ++i) {
			if(tranzactie.getSuma().compareTo(transactions.get(buttons.get(i).getKey()-1).getSuma())>0) {
				sidebarVBox.getChildren().add(i, newButton);
				notAdded=false;
			}
		}
		if(notAdded)
			sidebarVBox.getChildren().add(newButton);
		++counter;
		buttonSubject.attach(new ButtonObserver(newButton, buttonSubject));
		db.stopEntityManager();
		
		System.out.println("Transaction Added Successfully");
	}
	class TranzactieComparator implements Comparator {

		@Override
		public int compare(Object arg0, Object arg1) {
			return (((Tranzactie)(arg1)).getSuma()).compareTo(((Tranzactie)(arg0)).getSuma());
		}
    	
    }
    
    @FXML
    void showTransactions() {
    	activePane = createTransaction;
    	counter=1;
    	DatabaseUtil  db = DatabaseUtil.getInstance();
    	List<Tranzactie> transactions = new ArrayList<>();
    	List<Masina> listaMasini = db.entityManager.createNativeQuery("SELECT * FROM auto.masina", Masina.class).getResultList();
    	transactions = (List<Tranzactie>)db.entityManager.createNativeQuery("SELECT * FROM auto.tranzactie", Tranzactie.class).getResultList();
    	this.transactions=transactions;
    	JCBComboBox.valueProperty().addListener((obs, oldVal, newVal)->
    	JTPrice.setText(Double.toString((newVal.getPret()*1000+newVal.getTva()*newVal.getPret())/1000)+" RON"));
    	JCBComboBox.setItems(FXCollections.observableArrayList(listaMasini));
    	transactions.sort(new TranzactieComparator());
    	JCBComboBox.setConverter(new StringConverter<Masina>(){
    		@Override
    		public String toString(Masina masina) {
    			return masina.getMarca() + " " + masina.getModel();
    		}

			@Override
			public Masina fromString(String arg0) {
				return null;
			}
    		
    	});
    	JCBComboBox.setStyle("-fx-font-size:14px;"
    			+ "-fx-font-family:Lato;");
    	JCBComboBox.setPromptText("Alege o masina");
    	JFXButton newButton;
    	sidebarVBox.getChildren().clear();
    	for(Tranzactie transaction:transactions) {
			newButton= new JFXButton("Transaction "+counter);
			buttons.add(new Pair<Integer, JFXButton>(counter, newButton));
			newButton.setTextAlignment(TextAlignment.CENTER);
			//newButton.set
			newButton.setStyle(
					"-fx-text-fill: #fff;");
			newButton.setFont(Font.font("Helvetica", FontWeight.THIN, 22));
			newButton.setOnAction(new EventHandler<ActionEvent>() {
				final Tranzactie t=transaction;
				Client client = transaction.getClient();
				Masina masina = transaction.getMasina();
				Adresa adresa = client.getAdresa();
				@Override public void handle(ActionEvent e) {
					JTNumeClient2.setText(client.getNume() + " " +client.getPrenume());
					JTSerieBuletinClient2.setText(client.getSerieBuletin());
					JTNumarBuletinClient2.setText(client.getNumarBuletin());
					JTCNPClient2.setText(client.getCnp());
					JTNumarTelefonClient2.setText(client.getNumarTelefon());
					JTStradaClient2.setText(adresa.getStrada() + " " + adresa.getNumar());
					JTJudetClient2.setText(adresa.getJudet());
					JTLocalitateClient2.setText(adresa.getLocalitate());
					JTCodPostalClient2.setText(Integer.toString(adresa.getCodPostal()));
					JTMasina.setText(masina.getMarca() + " " +masina.getModel());
					JTCuloare.setText(masina.getCuloare());
					JTCapacitateCilindrica.setText(Integer.toString(masina.getCapacitateCilindrica()));
					JTCaiPutere.setText(Integer.toString(masina.getCaiPutere()));
					JTFactorPoluare.setText(masina.getFactorDePoluare());
					JTNumarUsi.setText(Integer.toString(masina.getNumarUsi()));
					JTNumarLocuri.setText(Integer.toString(masina.getNumarLocuri()));
					JTDataFabricatie.setText(masina.getDataFabricatie());
					JTPrice2.setText(t.getSuma() + "RON");
					if(activePane!=showTransaction) {
						showTransaction.toFront();
						showTransaction.setVisible(true);
						showTransaction.setDisable(false);
						activePane.setDisable(true);
						activePane.setVisible(false);
						activePane=showTransaction;
					}
				}
			});
			sidebarVBox.getChildren().add(newButton);
			++counter;
			buttonSubject.attach(new ButtonObserver(newButton, buttonSubject));
    	}
    	
    	db.stopEntityManager();
    	setSettingsPage();
    }
    
    void setSettingsPage() {
    	JISettingsIcon.setDisable(false);
    	JISettingsIcon.setVisible(true);
    	JBSettingsButton.toFront();
    	JBSettingsButton.setDisable(false);
    	JBSettingsButton.setVisible(true);
    	Integer [] fontSizes = new Integer [] {10, 12, 14, 16, 18, 20, 22, 24};
    	JCBFontSize.setItems(FXCollections.observableArrayList(Arrays.asList(fontSizes)));
    	JCBFontSize.setStyle("-fx-font-size:14px;"
    			+ "-fx-font-family:Lato;");
    }
    
    @FXML
    void saveChanges(){
    	updateSidebarColor();
    	buttonSubject.setFontSize(JCBFontSize.getSelectionModel().getSelectedItem());
    	buttonSubject.setColor(JCPColorPicker.getValue());
    	buttonSubject.notifyAllObservers();
    }
    
    @FXML
    void updateSidebarColor() {
    	sidebarColor = JCPSidebarColor.getValue();
    	JAPSidebar.setStyle("-fx-background-color: " + getHexColorString(sidebarColor) + ";");
    }
    
    @FXML
    void resetToDefaults(){
    	JAPSidebar.setStyle("-fx-background-color: " + getHexColorString(defaultSidebarColor) + ";");
    	buttonSubject.resetToDefaults();
    	buttonSubject.notifyAllObservers();
    }
    
    private static String getHexColorString(Color color) {
		return String.format("#%02X%02X%02X", (int)(color.getRed()*255),
				(int)(color.getGreen()*255),
				(int)(color.getBlue()*255));
    }
}
