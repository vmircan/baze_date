package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the stoc_masini database table.
 * 
 */
@Entity
@Table(name="stoc_masini")
@NamedQuery(name="StocMasini.findAll", query="SELECT s FROM StocMasini s")
public class StocMasini implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private int cantitate;

	@Temporal(TemporalType.DATE)
	@Column(name="data_receptionarii")
	private Date dataReceptionarii;

	//bi-directional many-to-one association to Masina
	@ManyToOne
	@JoinColumn(name="id_masina")
	private Masina masina;

	//bi-directional many-to-one association to Fabrica
	@ManyToOne
	@JoinColumn(name="id_fabrica")
	private Fabrica fabrica;

	public StocMasini() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCantitate() {
		return this.cantitate;
	}

	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}

	public Date getDataReceptionarii() {
		return this.dataReceptionarii;
	}

	public void setDataReceptionarii(Date dataReceptionarii) {
		this.dataReceptionarii = dataReceptionarii;
	}

	public Masina getMasina() {
		return this.masina;
	}

	public void setMasina(Masina masina) {
		this.masina = masina;
	}

	public Fabrica getFabrica() {
		return this.fabrica;
	}

	public void setFabrica(Fabrica fabrica) {
		this.fabrica = fabrica;
	}

}