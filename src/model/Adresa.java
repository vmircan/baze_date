package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the adresa database table.
 * 
 */
@Entity
@NamedQuery(name="Adresa.findAll", query="SELECT a FROM Adresa a")
public class Adresa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private int id;

	@Column(name="cod_postal")
	private int codPostal;

	private String judet;

	private String localitate;

	@Column(name="nr_apartament")
	private int nrApartament;

	private int numar;

	private String scara;

	private String strada;

	//bi-directional many-to-one association to Client
	@OneToMany(mappedBy="adresa")
	private List<Client> clients;

	//bi-directional many-to-one association to Fabrica
	@OneToMany(mappedBy="adresa")
	private List<Fabrica> fabricas;

	//bi-directional many-to-one association to Producator
	@OneToMany(mappedBy="adresa")
	private List<Producator> producators;

	public Adresa() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCodPostal() {
		return this.codPostal;
	}

	public void setCodPostal(int codPostal) {
		this.codPostal = codPostal;
	}

	public String getJudet() {
		return this.judet;
	}

	public void setJudet(String judet) {
		this.judet = judet;
	}

	public String getLocalitate() {
		return this.localitate;
	}

	public void setLocalitate(String localitate) {
		this.localitate = localitate;
	}

	public int getNrApartament() {
		return this.nrApartament;
	}

	public void setNrApartament(int nrApartament) {
		this.nrApartament = nrApartament;
	}

	public int getNumar() {
		return this.numar;
	}

	public void setNumar(int numar) {
		this.numar = numar;
	}

	public String getScara() {
		return this.scara;
	}

	public void setScara(String scara) {
		this.scara = scara;
	}

	public String getStrada() {
		return this.strada;
	}

	public void setStrada(String strada) {
		this.strada = strada;
	}

	public List<Client> getClients() {
		return this.clients;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public Client addClient(Client client) {
		getClients().add(client);
		client.setAdresa(this);

		return client;
	}

	public Client removeClient(Client client) {
		getClients().remove(client);
		client.setAdresa(null);

		return client;
	}

	public List<Fabrica> getFabricas() {
		return this.fabricas;
	}

	public void setFabricas(List<Fabrica> fabricas) {
		this.fabricas = fabricas;
	}

	public Fabrica addFabrica(Fabrica fabrica) {
		getFabricas().add(fabrica);
		fabrica.setAdresa(this);

		return fabrica;
	}

	public Fabrica removeFabrica(Fabrica fabrica) {
		getFabricas().remove(fabrica);
		fabrica.setAdresa(null);

		return fabrica;
	}

	public List<Producator> getProducators() {
		return this.producators;
	}

	public void setProducators(List<Producator> producators) {
		this.producators = producators;
	}

	public Producator addProducator(Producator producator) {
		getProducators().add(producator);
		producator.setAdresa(this);

		return producator;
	}

	public Producator removeProducator(Producator producator) {
		getProducators().remove(producator);
		producator.setAdresa(null);

		return producator;
	}

}