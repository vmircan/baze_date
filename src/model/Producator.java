package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the producator database table.
 * 
 */
@Entity
@NamedQuery(name="Producator.findAll", query="SELECT p FROM Producator p")
public class Producator implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Temporal(TemporalType.DATE)
	@Column(name="an_infiintare")
	private Date anInfiintare;

	@Column(name="nr_telefon")
	private String nrTelefon;

	private String nume;

	//bi-directional many-to-one association to Fabrica
	@OneToMany(mappedBy="producator")
	private List<Fabrica> fabricas;

	//bi-directional many-to-one association to Adresa
	@ManyToOne
	@JoinColumn(name="id_adresa")
	private Adresa adresa;

	public Producator() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getAnInfiintare() {
		return this.anInfiintare;
	}

	public void setAnInfiintare(Date anInfiintare) {
		this.anInfiintare = anInfiintare;
	}

	public String getNrTelefon() {
		return this.nrTelefon;
	}

	public void setNrTelefon(String nrTelefon) {
		this.nrTelefon = nrTelefon;
	}

	public String getNume() {
		return this.nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public List<Fabrica> getFabricas() {
		return this.fabricas;
	}

	public void setFabricas(List<Fabrica> fabricas) {
		this.fabricas = fabricas;
	}

	public Fabrica addFabrica(Fabrica fabrica) {
		getFabricas().add(fabrica);
		fabrica.setProducator(this);

		return fabrica;
	}

	public Fabrica removeFabrica(Fabrica fabrica) {
		getFabricas().remove(fabrica);
		fabrica.setProducator(null);

		return fabrica;
	}

	public Adresa getAdresa() {
		return this.adresa;
	}

	public void setAdresa(Adresa adresa) {
		this.adresa = adresa;
	}

}