package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the tranzactie database table.
 * 
 */
@Entity
@NamedQuery(name="Tranzactie.findAll", query="SELECT t FROM Tranzactie t")
public class Tranzactie implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private int id;

	private String data;

	@Column(name="perioada_garantie")
	private int perioadaGarantie;

	private String suma;

	//bi-directional many-to-one association to Masina
	@ManyToOne
	@JoinColumn(name="id_masina")
	private Masina masina;

	//bi-directional many-to-one association to Client
	@ManyToOne
	@JoinColumn(name="id_client")
	private Client client;

	public Tranzactie() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getData() {
		return this.data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public int getPerioadaGarantie() {
		return this.perioadaGarantie;
	}

	public void setPerioadaGarantie(int perioadaGarantie) {
		this.perioadaGarantie = perioadaGarantie;
	}

	public String getSuma() {
		return this.suma;
	}

	public void setSuma(String suma) {
		this.suma = suma;
	}

	public Masina getMasina() {
		return this.masina;
	}

	public void setMasina(Masina masina) {
		this.masina = masina;
	}

	public Client getClient() {
		return this.client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

}