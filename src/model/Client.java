package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the client database table.
 * 
 */
@Entity
@NamedQuery(name="Client.findAll", query="SELECT c FROM Client c")
public class Client implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private int id;

	private String cnp;

	@Column(name="numar_buletin")
	private String numarBuletin;

	@Column(name="numar_telefon")
	private String numarTelefon;

	private String nume;

	private String prenume;

	@Column(name="serie_buletin")
	private String serieBuletin;

	//bi-directional many-to-one association to Adresa
	@ManyToOne
	@JoinColumn(name="id_adresa")
	private Adresa adresa;

	//bi-directional many-to-one association to Tranzactie
	@OneToMany(mappedBy="client")
	private List<Tranzactie> tranzacties;

	public Client() {
	}
	
	public Client(String nume, String prenume, Adresa adresa, String CNP,
			String serieBuletin, String numarBuletin, String numarTelefon) {
		this.nume=nume;
		this.prenume=prenume;
		this.adresa=adresa;
		this.serieBuletin=serieBuletin;
		this.numarBuletin=numarBuletin;
		this.numarTelefon=numarTelefon;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCnp() {
		return this.cnp;
	}

	public void setCnp(String cnp) {
		this.cnp = cnp;
	}

	public String getNumarBuletin() {
		return this.numarBuletin;
	}

	public void setNumarBuletin(String numarBuletin) {
		this.numarBuletin = numarBuletin;
	}

	public String getNumarTelefon() {
		return this.numarTelefon;
	}

	public void setNumarTelefon(String numarTelefon) {
		this.numarTelefon = numarTelefon;
	}

	public String getNume() {
		return this.nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getPrenume() {
		return this.prenume;
	}

	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}

	public String getSerieBuletin() {
		return this.serieBuletin;
	}

	public void setSerieBuletin(String serieBuletin) {
		this.serieBuletin = serieBuletin;
	}

	public Adresa getAdresa() {
		return this.adresa;
	}

	public void setAdresa(Adresa adresa) {
		this.adresa = adresa;
	}

	public List<Tranzactie> getTranzacties() {
		return this.tranzacties;
	}

	public void setTranzacties(List<Tranzactie> tranzacties) {
		this.tranzacties = tranzacties;
	}

	public Tranzactie addTranzacty(Tranzactie tranzacty) {
		getTranzacties().add(tranzacty);
		tranzacty.setClient(this);

		return tranzacty;
	}

	public Tranzactie removeTranzacty(Tranzactie tranzacty) {
		getTranzacties().remove(tranzacty);
		tranzacty.setClient(null);

		return tranzacty;
	}

}