package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the masina database table.
 * 
 */
@Entity
@NamedQuery(name="Masina.findAll", query="SELECT m FROM Masina m")
public class Masina implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Column(name="cai_putere")
	private int caiPutere;

	@Column(name="capacitate_cilindrica")
	private int capacitateCilindrica;

	private String culoare;

	@Column(name="data_fabricatie")
	private String dataFabricatie;

	@Column(name="factor_de_poluare")
	private String factorDePoluare;

	private String marca;

	private String model;

	@Column(name="numar_locuri")
	private int numarLocuri;

	@Column(name="numar_usi")
	private int numarUsi;

	private int pret;

	private int tva;

	//bi-directional many-to-one association to StocMasini
	@OneToMany(mappedBy="masina")
	private List<StocMasini> stocMasinis;

	//bi-directional many-to-one association to Tranzactie
	@OneToMany(mappedBy="masina")
	private List<Tranzactie> tranzacties;

	public Masina() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCaiPutere() {
		return this.caiPutere;
	}

	public void setCaiPutere(int caiPutere) {
		this.caiPutere = caiPutere;
	}

	public int getCapacitateCilindrica() {
		return this.capacitateCilindrica;
	}

	public void setCapacitateCilindrica(int capacitateCilindrica) {
		this.capacitateCilindrica = capacitateCilindrica;
	}

	public String getCuloare() {
		return this.culoare;
	}

	public void setCuloare(String culoare) {
		this.culoare = culoare;
	}

	public String getDataFabricatie() {
		return this.dataFabricatie;
	}

	public void setDataFabricatie(String dataFabricatie) {
		this.dataFabricatie = dataFabricatie;
	}

	public String getFactorDePoluare() {
		return this.factorDePoluare;
	}

	public void setFactorDePoluare(String factorDePoluare) {
		this.factorDePoluare = factorDePoluare;
	}

	public String getMarca() {
		return this.marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModel() {
		return this.model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getNumarLocuri() {
		return this.numarLocuri;
	}

	public void setNumarLocuri(int numarLocuri) {
		this.numarLocuri = numarLocuri;
	}

	public int getNumarUsi() {
		return this.numarUsi;
	}

	public void setNumarUsi(int numarUsi) {
		this.numarUsi = numarUsi;
	}

	public int getPret() {
		return this.pret;
	}

	public void setPret(int pret) {
		this.pret = pret;
	}

	public int getTva() {
		return this.tva;
	}

	public void setTva(int tva) {
		this.tva = tva;
	}

	public List<StocMasini> getStocMasinis() {
		return this.stocMasinis;
	}

	public void setStocMasinis(List<StocMasini> stocMasinis) {
		this.stocMasinis = stocMasinis;
	}

	public StocMasini addStocMasini(StocMasini stocMasini) {
		getStocMasinis().add(stocMasini);
		stocMasini.setMasina(this);

		return stocMasini;
	}

	public StocMasini removeStocMasini(StocMasini stocMasini) {
		getStocMasinis().remove(stocMasini);
		stocMasini.setMasina(null);

		return stocMasini;
	}

	public List<Tranzactie> getTranzacties() {
		return this.tranzacties;
	}

	public void setTranzacties(List<Tranzactie> tranzacties) {
		this.tranzacties = tranzacties;
	}

	public Tranzactie addTranzacty(Tranzactie tranzacty) {
		getTranzacties().add(tranzacty);
		tranzacty.setMasina(this);

		return tranzacty;
	}

	public Tranzactie removeTranzacty(Tranzactie tranzacty) {
		getTranzacties().remove(tranzacty);
		tranzacty.setMasina(null);

		return tranzacty;
	}

}