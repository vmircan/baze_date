package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the fabrica database table.
 * 
 */
@Entity
@NamedQuery(name="Fabrica.findAll", query="SELECT f FROM Fabrica f")
public class Fabrica implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	//bi-directional many-to-one association to Adresa
	@ManyToOne
	@JoinColumn(name="id_adresa")
	private Adresa adresa;

	//bi-directional many-to-one association to Producator
	@ManyToOne
	@JoinColumn(name="id_producator")
	private Producator producator;

	//bi-directional many-to-one association to StocMasini
	@OneToMany(mappedBy="fabrica")
	private List<StocMasini> stocMasinis;

	public Fabrica() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Adresa getAdresa() {
		return this.adresa;
	}

	public void setAdresa(Adresa adresa) {
		this.adresa = adresa;
	}

	public Producator getProducator() {
		return this.producator;
	}

	public void setProducator(Producator producator) {
		this.producator = producator;
	}

	public List<StocMasini> getStocMasinis() {
		return this.stocMasinis;
	}

	public void setStocMasinis(List<StocMasini> stocMasinis) {
		this.stocMasinis = stocMasinis;
	}

	public StocMasini addStocMasini(StocMasini stocMasini) {
		getStocMasinis().add(stocMasini);
		stocMasini.setFabrica(this);

		return stocMasini;
	}

	public StocMasini removeStocMasini(StocMasini stocMasini) {
		getStocMasinis().remove(stocMasini);
		stocMasini.setFabrica(null);

		return stocMasini;
	}

}